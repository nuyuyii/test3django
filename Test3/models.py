from django.db import models


class CreateQ(models.Model):
    question = models.TextField(default='')
    choice1 = models.TextField(default='')
    choice2 = models.TextField(default='')
    choice3 = models.TextField(default='')
    choice4 = models.TextField(default='')
    pub_date = models.TextField(default='')


class Answer(models.Model):
    ques = models.TextField(default='')
    answer = models.TextField(default='')
    pub_date = models.TextField(default='')


class YourAnswer(models.Model):
    question = models.TextField(default='')
    answer = models.TextField(default='')
    pub_date = models.TextField(default='')
