# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Test3', '0005_remove_item_pub_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='answer',
        ),
        migrations.AddField(
            model_name='item',
            name='answer1',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='answer2',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='answer3',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='answer4',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='item',
            name='question',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
