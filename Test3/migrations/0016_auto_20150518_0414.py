# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('Test3', '0015_youranswer'),
    ]

    operations = [
        migrations.AddField(
            model_name='answer',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 18, 4, 14, 37, 914917, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='createq',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 18, 4, 14, 37, 914171, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='youranswer',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 18, 4, 14, 37, 915606, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
