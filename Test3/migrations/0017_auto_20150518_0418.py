# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('Test3', '0016_auto_20150518_0414'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 18, 4, 18, 28, 302715, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='createq',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 18, 4, 18, 28, 301951, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='youranswer',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 18, 4, 18, 28, 303406, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
