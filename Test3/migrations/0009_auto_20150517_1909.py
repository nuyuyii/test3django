# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Test3', '0008_auto_20150515_1543'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Create',
            new_name='CreateQ',
        ),
    ]
