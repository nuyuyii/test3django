# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Test3', '0014_remove_answer_question'),
    ]

    operations = [
        migrations.CreateModel(
            name='YourAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('question', models.TextField(default='')),
                ('answer', models.TextField(default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
