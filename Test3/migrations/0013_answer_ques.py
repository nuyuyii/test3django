# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Test3', '0012_auto_20150517_1931'),
    ]

    operations = [
        migrations.AddField(
            model_name='answer',
            name='ques',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
