# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('Test3', '0003_auto_20150514_0741'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='answer',
            field=models.TextField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='item',
            name='pub_date',
            field=models.DateTimeField(verbose_name=datetime.datetime(2015, 5, 14, 8, 4, 47, 643490, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
