# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Test3', '0006_auto_20150515_0420'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='answer1',
            new_name='choice1',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='answer2',
            new_name='choice2',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='answer3',
            new_name='choice3',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='answer4',
            new_name='choice4',
        ),
    ]
