# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Survey',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('question', models.TextField()),
                ('answer', models.TextField()),
                ('pub_date', models.DateTimeField(verbose_name=datetime.datetime(2015, 5, 14, 6, 19, 16, 503002, tzinfo=utc))),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
