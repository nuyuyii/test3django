# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('Test3', '0017_auto_20150518_0418'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 18, 4, 28, 12, 145489, tzinfo=utc)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='createq',
            name='pub_date',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='youranswer',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 18, 4, 28, 12, 146130, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
