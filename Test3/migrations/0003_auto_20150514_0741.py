# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('Test3', '0002_auto_20150514_0620'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='answer',
            field=models.IntegerField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='item',
            name='pub_date',
            field=models.DateTimeField(verbose_name=datetime.datetime(2015, 5, 14, 7, 41, 33, 748598, tzinfo=utc)),
            preserve_default=True,
        ),
    ]
