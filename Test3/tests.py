from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase

from Test3.models import CreateQ, Answer, YourAnswer
from Test3.views import home_page, Create_page, ShowAnswer_page, ShowChoice_page, history_page, select_page
import unittest

from django.utils import timezone
import datetime
import pytz

class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)


    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.status_code, 200)
        # self.assertEqual(response.content.decode(), expected_html)


class CreateQuestionTest(TestCase):

    def test_create_a_POST_request_and_redirects_after_POST(self):
        # ไปที่หน้า create question and choice
        # เช็คว่าไปถูกหน้า
        # ทดสอบการเก็บคำถามนั้น ใช้ name
        url = '/CreateQ&C/'
        response = self.client.get(url)
        # self.assertEqual(response.status_code, 200)
        response = self.client.post(url, {'ask': 'He said',
                                          'ans1': 'one',
                                          'ans2': 'two',
                                          'ans3': 'three',
                                          'ans4': 'four',
                                          'create_send': 'create'})
        self.assertEqual(CreateQ.objects.count(), 1)
        # check ข้อความถูกบันทึก
        create_ques = CreateQ.objects.first()
        self.assertEqual(create_ques.question, 'He said')
        self.assertEqual(create_ques.choice1, 'one')
        # update time
        date_create = datetime.datetime.now()
        date_test_create = date_create.strftime('%Y-%m-%d %I:%M %p')
        self.assertEqual(create_ques.pub_date, date_test_create)
        # redirect to http://0.0.0.0:8000
        # check ข้อความปรากฏที่หน้าแรก
        self.assertRedirects(response, '/')
        response = self.client.get('/')
        self.assertContains(response, 'He said')


class SelectChoiceTest(TestCase):

    def test_uses_select_choice_complete(self):
        # เริ่มโดยการเพิ่มคำถามและคำตอบ
        self.client.post(
            '/CreateQ&C/',
            {'ask': 'He said',
             'ans1': 'one',
             'ans2': 'two',
             'ans3': 'three',
             'ans4': 'four',
             'create_send': 'create'}
        )
        self.client.post(
            '/CreateQ&C/',
            {'ask': 'She said',
             'ans1': 'first',
             'ans2': 'second',
             'ans3': 'third',
             'ans4': 'fourth',
             'create_send': 'create'}
        )
        self.assertEqual(CreateQ.objects.count(), 2)
        create_ques1 = CreateQ.objects.first()
        # ไปที่หน้าแรก เลือกคำตอบของทั้งสองคำถาม
        # จากนั้นส่งคำตอบ
        # ตรวจสอบ Redirect ไปที่หน้าคำตอบทั้งหมดที่คุณเลือกหรือไม่
        response = self.client.get('/')
        response = self.client.post('/',
                               {'He said': 'two',
                                'She said': 'first',
                                'ans_send': 'Answer'})
        self.assertRedirects(response, '/ShowAnswer/')
        # ตรวจตัวเลือกที่ผู้ใช้เลือกว่าถูกบันทึกในคำตอบของคุณหรือไม่
        self.assertEqual(YourAnswer.objects.count(), 2)
        saved_your_answer = YourAnswer.objects.all()
        your_answerQ1 = saved_your_answer[0]
        self.assertEqual(your_answerQ1.question, 'He said')
        self.assertEqual(your_answerQ1.answer, 'two')
        your_answerQ2 = saved_your_answer[1]
        self.assertEqual(your_answerQ2.question, 'She said')
        self.assertEqual(your_answerQ2.answer, 'first')
        # ตรวจคำตอบที่ถูกแสดง
        response = self.client.get('/ShowAnswer/')
        self.assertContains(response, 'He said')
        self.assertContains(response, 'She said')
        self.assertNotContains(response, 'one')
        self.assertNotContains(response, 'second')


    def test_uses_select_choice_error(self):
        # เริ่มโดยการเพิ่มคำถามและคำตอบ
        self.client.post(
            '/CreateQ&C/',
            {'ask': 'She said',
             'ans1': 'first',
             'ans2': 'second',
             'ans3': 'third',
             'ans4': 'fourth',
             'create_send': 'create'}
        )
        # ส่งคำตอบ โดยไม่เลือกคำตอบข้อใดข้อหนึ่ง
        # ตรวจสอบ Redirect ไปที่หน้าให้กรอกคำตอบทั้งหมดหรือไม่
        response = self.client.get('/')
        response = self.client.post('/',
                               {'ans_send': 'Answer'})
        self.assertRedirects(response, '/nonselect/')
        response = self.client.get('/nonselect/')
        self.assertContains(response, 'Please select all fields.')

    def test_select_total_choice_in_question(self):
        # ทดสอบคำถามและคำตอบที่เคยถูกตอบทั้งหมด
        # และจะทดสอบว่าในคำถามนั้นๆ มีคำตอบที่เคยถูกเลือกทั้งหมดกี่ครั้ง
        # เริ่มโดยการเพิ่มคำถามและคำตอบ
        self.client.post(
            '/CreateQ&C/',
            {'ask': 'He said',
             'ans1': 'one',
             'ans2': 'two',
             'ans3': 'three',
             'ans4': 'four',
             'create_send': 'create'}
        )
        self.client.post(
            '/CreateQ&C/',
            {'ask': 'She said',
             'ans1': 'first',
             'ans2': 'second',
             'ans3': 'third',
             'ans4': 'fourth',
             'create_send': 'create'}
        )
        self.assertEqual(CreateQ.objects.count(), 2)
        create_ques1 = CreateQ.objects.first()
        # ไปที่หน้าแรก เลือกคำตอบของทั้งสองคำถาม ทำสองครั้ง
        response = self.client.get('/')
        response = self.client.post('/',
                               {'He said': 'two',
                                'She said': 'first',
                                'ans_send': 'Answer'})
        self.assertRedirects(response, '/ShowAnswer/')
        response = self.client.post('/',
                               {'He said': 'one',
                                'She said': 'second',
                                'ans_send': 'Answer'})
        self.assertRedirects(response, '/ShowAnswer/')
        # check คำตอบที่เคยถูกเลือกถูกบันทึกไว้ทั้งหมดและถูกต้อง
        saved_answer = Answer.objects.all()
        answerQ1 = saved_answer[0]
        answerQ2 = saved_answer[1]
        answerQ3 = saved_answer[2]
        answerQ4 = saved_answer[3]
        self.assertEqual(answerQ1.ques, 'He said')
        self.assertEqual(answerQ1.answer, 'two')
        self.assertEqual(answerQ2.ques, 'She said')
        self.assertEqual(answerQ2.answer, 'first')
        self.assertEqual(answerQ3.ques, 'He said')
        self.assertEqual(answerQ3.answer, 'one')
        self.assertEqual(answerQ4.ques, 'She said')
        self.assertEqual(answerQ4.answer, 'second')
        # ไปที่หน้าแสดงคำตอบของคำถามข้อที่ 1
        # แสดงคำตอบที่เคยถูกเลือกของคำถามนั้น
        url = '/ShowChoice/1/'
        response = self.client.get(url)
        self.assertContains(response, 'He said')
        self.assertContains(response, 'one: 1 คน')
        self.assertNotContains(response, 'four: 1 คน')


class CreateQModelTest(TestCase):

    def test_saving_question_and_choice(self):
        first_text = CreateQ()
        first_text.question = 'He said'
        first_text.choice1 = 'one'
        first_text.choice2 = 'two'
        first_text.choice3 = 'three'
        first_text.choice4 = 'four'
        # update time
        date_create = datetime.datetime.now()
        date_test_create = date_create.strftime('%Y-%m-%d %I:%M %p')
        first_text.pub_date = date_test_create
        first_text.save()

        second_text = CreateQ()
        second_text.question = 'She said'
        second_text.choice1 = 'first'
        second_text.choice2 = 'second'
        second_text.choice3 = 'third'
        second_text.choice4 = 'fourth'
        # update time
        date_create = datetime.datetime.now()
        date_test_create = date_create.strftime('%Y-%m-%d %I:%M %p')
        second_text.pub_date = date_test_create
        second_text.save()

        saved_text = CreateQ.objects.all()
        self.assertEqual(saved_text.count(), 2)

        first_saved_text = saved_text[0]
        self.assertEqual(first_saved_text.question, 'He said')
        self.assertEqual(first_saved_text.choice1, 'one')
        self.assertEqual(first_saved_text.choice2, 'two')
        self.assertEqual(first_saved_text.choice3, 'three')
        self.assertEqual(first_saved_text.choice4, 'four')
        self.assertEqual(first_saved_text.pub_date, date_test_create)
        second_saved_text = saved_text[1]
        self.assertEqual(second_saved_text.question, 'She said')
        self.assertEqual(second_saved_text.choice1, 'first')
