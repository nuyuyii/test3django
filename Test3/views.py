from django.shortcuts import redirect, render
from Test3.models import CreateQ, Answer, YourAnswer
from django.forms import ModelForm
import pytz
import datetime
from django.utils import timezone
from django.utils.timezone import localtime

def home_page(request):
    questions = CreateQ.objects.all()
    whole_ask = []
    for create in questions:
        whole_ask.append(create.question)

    if(request.method == 'POST'and request.POST.get(
                                   'ans_send','') == 'Answer'):
        YourAnswer.objects.all().delete() #delete database
        for i in range(len(whole_ask)):
            # ans_text = request.POST.get(whole_ask[i], False)
            # ทำให้เป็นเงื่อนไข ต้องตอบคำถามให้ครบ
            if whole_ask[i] in request.POST:
                ans_text = request.POST[whole_ask[i]]
            else:
                ans_text = False
                return redirect('/nonselect/')
            ans_text = request.POST[whole_ask[i]]
            date = datetime.datetime.now()
            date_text = date.strftime('%Y-%m-%d %I:%M %p')
            Answer.objects.create(
                            ques=whole_ask[i],
                            answer=ans_text,
                            pub_date=date_text
                           )
            YourAnswer.objects.create(
                            question=whole_ask[i],
                            answer=ans_text,
                            pub_date=date_text
                           )
        if YourAnswer.objects.count()==CreateQ.objects.count():
            return redirect('/ShowAnswer/')

    if(request.method == 'POST'and request.POST.get(
                                   'Create_send','') == 'Create'):
        return redirect('/CreateQ&C/')
    Answers = CreateQ.objects.count()
    return render(request, 'home.html',
                  {'items': questions, 'answers': Answers})

# สร้างคำถามและคำตอบ
def Create_page(request):
    questions = CreateQ.objects.all()
    if(request.method == 'POST'and request.POST.get(
                                   'create_send','') == 'create'):
        ask_text = request.POST['ask']
        ans1_text = request.POST['ans1']
        ans2_text = request.POST['ans2']
        ans3_text = request.POST['ans3']
        ans4_text = request.POST['ans4']
        date = datetime.datetime.now()
        date_text = date.strftime('%Y-%m-%d %I:%M %p')
        CreateQ.objects.create(
                          question=ask_text,
                          choice1=ans1_text,
                          choice2=ans2_text,
                          choice3=ans3_text,
                          choice4=ans4_text,
                          pub_date=date_text
                         )
        return redirect('/')
    return render(request, 'CreateQ&C.html',
                  {'items': questions})

# แสดงคำตอบที่คุณได้ตอบคำถามไปล่าสุด
def ShowAnswer_page(request):
    questions = CreateQ.objects.all()
    Answers = YourAnswer.objects.all()
    date = []
    if YourAnswer.objects.count()!=0:
        for create in Answers:
            date.append(create.pub_date)
    else:
        date[0:1] = [0,1]  # ให้ array data date เก็บค่า 0

    return render(request, 'ShowAnswer.html',
                  {'answers': Answers, 'pub_date': date[0],
                   'items': questions})

# แสดงคำตอบทั้งหมดที่ user เคยมาตอบคำถามในข้อนั้น
def ShowChoice_page(request, question_id):
    ques = CreateQ.objects.get(pk=question_id)
    question = CreateQ.objects.all()
    anstotal = Answer.objects.all()
    num1=0
    num2=0
    num3=0
    num4=0
    id_Q = int(question_id)-1
    whole_ans = []
    for create in anstotal:
        whole_ans.append(create.answer)

    ch1 = []
    ch2 = []
    ch3 = []
    ch4 = []
    for q in question:
        ch1.append(q.choice1)
        ch2.append(q.choice2)
        ch3.append(q.choice3)
        ch4.append(q.choice4)

    # เปรียบเทียบ โดยให้ ch1[id_Q] หรือคือ choice1 ของคำถามนี้
    # บวกค่า num1 เพิ่ม เมื่อมีคำตอบตรงกับ choice ที่ 1
    if Answer.objects.count() != 0:
        for i in range(len(whole_ans)):
            if ( ch1[id_Q]==whole_ans[i]):
                num1 = num1+1
            elif ( ch2[id_Q]==whole_ans[i]):
                num2 = num2+1
            elif ( ch3[id_Q]==whole_ans[i]):
                num3 = num3+1
            elif ( ch4[id_Q]==whole_ans[i]):
                num4 = num4+1

    return render(request, 'ShowChoice.html',
                  {'each_Q': ques, 'num1': num1,
                   'num2': num2, 'num3': num3,
                   'num4': num4, 'items': question})

def history_page(request):    
    Answers = Answer.objects.all()
    questions = CreateQ.objects.all()

    if request.method == 'POST' and request.POST.get(
                                    'sortbyques', '') == 'Sort by ques':
        Answers = Answer.objects.all().order_by('ques')
    if request.method == 'POST' and request.POST.get(
                                    'sortbydate', '') == 'Sort by dates':
        Answers = Answer.objects.all().order_by('-pub_date')

    count = Answer.objects.count()
    return render(request, 'history.html',
                  {'answers': Answers, 'items': questions,
                   'counts': count})

def select_page(request):

    return render(request, 'nonselect.html')
