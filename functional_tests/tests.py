from django.test import LiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.utils import timezone
import datetime
import pytz
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions


class NewVisitorTest(LiveServerTestCase):

    # classmethod กำหนดให้ browser ของ web นี้เป็น Firefox() จากที่ import 
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(2)

    # classmethod กำหนดให้ เมื่อจบการทดสอบก็ปิด browser 
    def tearDown(self):
        self.browser.quit()

    # check row table id 'id_choicesTable'
    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_choicesTable')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_create_and_select_a_choice_in_question_it_later(self):
        # เปิดหน้าเว็บขึ้นมา จากค่าเริ่มต้นของ server's address 'localhost:8081'
        # ที่เป็น url แบบเต็มที่สามารถเข้าถึงในระหว่างการทดสอบ
        # self.live_server_url หากคุณต้องการที่จะเปลี่ยนแปลงที่อยู่เริ่มต้น 
        # (in the case, for example, Where the 8081 port is already taken)
        # แล้วคุณต้องการผ่านไปยังที่อยู่ที่แตกต่างอีกที่หนึ่ง
        # จะใช้คำสั่งการทดสอบผ่านตัวเลือก --liveserver 
        self.browser.get(self.live_server_url)
        # ให้ browser เข้าไปที่หน้าสร้างคำถามและคำตอบ createQ&C
        # เราจะเริ่มที่การตั้งคำถามและคำตอบ
        self.browser.get("http://localhost:8081/CreateQ&C/")

        # check title and check header
        self.assertIn('Question and Choice Create', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Question and Choice Create', header_text)

        # ให้ inputboxQuestions ร้องขอ question input box 
        # id from browser and check placeholder
        inputboxQuestions = self.browser.find_element_by_id('id_ask')
        self.assertEqual(
                inputboxQuestions.get_attribute('placeholder'),
                'Enter your question'
        )
        # ให้ inputboxChoice1 ร้องขอ choice1 input box 
        # id from browser and check placeholder
        inputboxChoice1 = self.browser.find_element_by_id('id_ans1')
        self.assertEqual(
                inputboxChoice1.get_attribute('placeholder'),
                'Enter your choice1'
        )
        # ให้ inputboxChoice3 ร้องขอ choice2 input box 
        # id from browser and check placeholder
        inputboxChoice2 = self.browser.find_element_by_id('id_ans2')
        self.assertEqual(
                inputboxChoice2.get_attribute('placeholder'),
                'Enter your choice2'
        )
        # ให้ inputboxChoice3 ร้องขอ choice3 input box 
        # id from browser and check placeholder
        inputboxChoice3 = self.browser.find_element_by_id('id_ans3')
        self.assertEqual(
                inputboxChoice3.get_attribute('placeholder'),
                'Enter your choice3'
        )
        # ให้ inputboxChoice4 ร้องขอ choice4 input box 
        # id from browser and check placeholder
        inputboxChoice4 = self.browser.find_element_by_id('id_ans4')
        self.assertEqual(
                inputboxChoice4.get_attribute('placeholder'),
                'Enter your choice4'
        )

        # สร้างคำถามและตัวเลือกใหม่ขึ้นมา ครั้งที่ 1
        inputboxQuestions = self.browser.find_element_by_id('id_ask')
        inputboxChoice1 = self.browser.find_element_by_id('id_ans1')
        inputboxChoice2 = self.browser.find_element_by_id('id_ans2')
        inputboxChoice3 = self.browser.find_element_by_id('id_ans3')
        inputboxChoice4 = self.browser.find_element_by_id('id_ans4')
        inputboxQuestions.send_keys('ที่ไหนเอ๋ย ที่คุณชอบไป')
        inputboxChoice1.send_keys('เข้าวัด')
        inputboxChoice2.send_keys('ข้าวสาร')
        inputboxChoice3.send_keys('เข้าซอย')
        inputboxChoice4.send_keys('เข้าป่า')
        # button รับค่าจาก browser id 'id_create'
        button = self.browser.find_element_by_id('id_create').click()
        date_create = datetime.datetime.now()
        date_test_create = date_create.strftime('%Y-%m-%d %I:%M %p')

        # When we hits enter, we is taken to a new URL
        # เมื่อกด create than check url ว่าเปลี่ยนแปลงถูกหรือไม่
        # ในที่นี้คือไปที่ lacalhost:8081 ซึ่งถูกต้อง
        edith_url = self.browser.current_url
        self.assertRegex(edith_url, '/')

        # เช็คข้อความที่เราใส่ลงไป ปรากฏในตาราง
        self.check_for_row_in_list_table('1. ที่ไหนเอ๋ย ที่คุณชอบไป')
        self.check_for_row_in_list_table('เข้าวัด')
        self.check_for_row_in_list_table('ข้าวสาร')
        self.check_for_row_in_list_table('เข้าซอย')
        self.check_for_row_in_list_table('เข้าป่า')
        self.check_for_row_in_list_table(date_test_create)
        time.sleep(2)

        # ตรวจการเลือกคำคอบ ครั้งที่ 1
        # เช็คผลของคำตอบมีส่งคำตอบลลิ้งไปหน้า your answer หรือไม่
        # เมื่อกลับหน้าแรก
        edith_url = self.browser.current_url
        self.assertRegex(edith_url, '/')
        button = self.browser.find_element_by_id('เข้าป่า').click()
        time.sleep(3)
        button = self.browser.find_element_by_id('id_ans').click()
        # set time when select choice
        date_select = datetime.datetime.now()
        date_test_select = date_select.strftime('%Y-%m-%d %I:%M %p')
        time.sleep(3)
        # when click send answer check url 
        send_ans_url = self.browser.current_url
        self.assertRegex(send_ans_url, 'http://localhost:8081/ShowAnswer/')
        # check head title page
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('YOUR ANSWER', header_text)
        # ตรวจดูข้อความจากการเพิ่มคำถามและตัวเลือกอีกครั้งใน tag 'body'
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('ที่ไหนเอ๋ย ที่คุณชอบไป', page_text)
        self.assertIn('เข้าป่า', page_text)
        self.assertNotIn('เข้าซอย', page_text)
        self.assertNotIn('เข้าวัด', page_text)
        # เช็คเวลาที่เมื่อตอบคำถาม
        self.assertIn('your answer :'+date_test_select, page_text)

        #### เช็คเมื่อกดปุ่ม total ที่อยู่ด้านข้างดูคะแนนของแต่ละข้อ ครั้งที่ 1
        # ให้กลับมาที่หน้าแรกก่อน
        self.browser.get(self.live_server_url)
        time.sleep(2)
        button = self.browser.find_element_by_id('view_question1').click()
        time.sleep(3)
        # check head title page
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Question and Choice score', header_text)
        # check score
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('Question: ที่ไหนเอ๋ย ที่คุณชอบไป', page_text)
        self.assertIn('เข้าป่า: 1 คน', page_text)
        self.assertIn('เข้าวัด: 0 คน', page_text)
        self.assertNotIn('ข้าวสาร: 1 คน', page_text)
        # เช็คเวลาที่เมื่อสร้างคำถาม
        self.assertIn(date_test_create, page_text)
        time.sleep(2)

        # บันทึกคำถามกับคำตอบ และ check การบันทึกอีก ครั้งที่ 2
        # เราจะดูการคงอยู่ของคำถามและตัวเลือกที่เราสร้างไว้
        # เมื่อเราเพิ่มคำถามใหม่เข้าไป
        # ให้ตัวแปร รับค่าจาก browser ตรงตาม id นั้น
        # แล้วสร้างคำถามและตัวเลือกใหม่ขึ้นมา
        self.browser.get("http://localhost:8081/CreateQ&C/")
        inputboxQuestions = self.browser.find_element_by_id('id_ask')
        inputboxChoice1 = self.browser.find_element_by_id('id_ans1')
        inputboxChoice2 = self.browser.find_element_by_id('id_ans2')
        inputboxChoice3 = self.browser.find_element_by_id('id_ans3')
        inputboxChoice4 = self.browser.find_element_by_id('id_ans4')
        inputboxQuestions.send_keys('มี ปู ปลา น้ำเต้า ไก่ กุ้ง เสือ')
        inputboxChoice1.send_keys('กุ้ง กับ ปลา')
        inputboxChoice2.send_keys('น้ำเต้า กับ เสือ')
        inputboxChoice3.send_keys('เสือเพียวๆ')
        inputboxChoice4.send_keys('ปูเอาไปสิบ')
        time.sleep(1)
        button = self.browser.find_element_by_id('id_create').click()
        # set time when click create
        date_create2 = datetime.datetime.now()
        date_test_create2 = date_create2.strftime('%Y-%m-%d %I:%M %p')

        # เลือกคำตอบอีกคครั้ง 2
        # เช็คผลของคำตอบมีส่งคำตอบลลิ้งไปหน้า your answer หรือไม่
        edith_url = self.browser.current_url
        self.assertRegex(edith_url, '/')
        button = self.browser.find_element_by_id('เข้าป่า').click()
        button = self.browser.find_element_by_id('เสือเพียวๆ').click()
        time.sleep(3)
        button = self.browser.find_element_by_id('id_ans').click()
        # set time when select choice ครั้งที่ 2
        date_select2 = datetime.datetime.now()
        date_test_select2 = date_select2.strftime('%Y-%m-%d %I:%M %p')
        time.sleep(3)
        # when click send answer check url 
        send_ans_url = self.browser.current_url
        self.assertRegex(send_ans_url, 'http://localhost:8081/ShowAnswer/')
        # check head title page
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('YOUR ANSWER', header_text)
        # check answer
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('ที่ไหนเอ๋ย ที่คุณชอบไป', page_text)
        self.assertIn('เข้าป่า', page_text)
        self.assertIn('มี ปู ปลา น้ำเต้า ไก่ กุ้ง เสือ', page_text)
        self.assertIn('เสือเพียวๆ', page_text)
        self.assertNotIn('เข้าซอย', page_text)
        self.assertNotIn('ปูเอาไปสิบ', page_text)
        # เช็คเวลาที่เมื่อตอบคำถาม ครั้งที่ 2
        self.assertIn('your answer :'+date_test_select2, page_text)

        # บันทึกคำถามกับคำตอบ และ check การบันทึกอีก ครั้งที่ 3 
        # กลับไปที่หน้า Home ก่อน แล้วเลือกคำตอบ
        # เมื่อตอบคำถามไม่ครบ ขาดคำตอบของ "ที่ไหนเอ๋ย ที่คุณชอบไป"
        self.browser.get(self.live_server_url)
        button = self.browser.find_element_by_id('เสือเพียวๆ').click()
        time.sleep(2)
        button = self.browser.find_element_by_id('id_ans').click()
        send_ans_url = self.browser.current_url
        self.assertRegex(send_ans_url, 'http://localhost:8081/nonselect/')
        header_text = self.browser.find_element_by_tag_name('h2').text
        self.assertIn('Please select all fields.', header_text)
        time.sleep(2)

        #### เราจะปิดและเปิด browser ขึ้นมาใหม่อีกครั้งเพื่อความแน่ใจว่าข้อมูลของเรายังอยู่
        #### ดูสิ่งที่ผู้ใช้เคยตอบคำถาม
        self.browser.quit()
        self.browser = webdriver.Firefox()
        # visit the home page ดูว่าเราไม่ได้แก้ไขรายการคำถามที่ตั้งไว้
        self.browser.get(self.live_server_url)
        #### เช็คเมื่อกดปุ่ม total ที่อยู่ด้านข้างดูคะแนนของแต่ละข้อ ครั้งที่ 2
        # ให้กลับมาที่หน้าแรกก่อน
        self.browser.get(self.live_server_url)
        time.sleep(2)
        button = self.browser.find_element_by_id('view_question1').click()
        time.sleep(3)
        # check head title page
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Question and Choice score', header_text)
        # check score
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('Question: ที่ไหนเอ๋ย ที่คุณชอบไป', page_text)
        self.assertIn('เข้าป่า: 2 คน', page_text)
        self.assertIn('เข้าวัด: 0 คน', page_text)
        self.assertNotIn('ข้าวสาร: 1 คน', page_text)
        # เช็คเวลาที่เมื่อสร้างคำถาม
        self.assertIn(date_test_create, page_text)
        time.sleep(2)

        #### check when click history
        #### ที่อยู่เมนูข้างบน
        self.browser.get("http://localhost:8081/history/")
        # check head title page
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Question and Choice History', header_text)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertIn('ที่ไหนเอ๋ย ที่คุณชอบไป', page_text)
        self.assertIn('เข้าป่า', page_text)
        self.assertIn('มี ปู ปลา น้ำเต้า ไก่ กุ้ง เสือ', page_text)
        self.assertIn(date_test_select, page_text)
        self.assertIn(date_test_select2, page_text)
        time.sleep(3)
        self.fail('Finish the test!')

        #### เวลาทำให้ error unsupported operand type(s) for +=: 'NoneType' and 'str' ยังไม่แก้ไข
