from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                       url(r'^$', 'Test3.views.home_page', name='home'),
                       url(r'CreateQ&C/$',
                           'Test3.views.Create_page',
                           name='CreateQ&C'),
                       url(r'ShowChoice/(\d+)/$',
                           'Test3.views.ShowChoice_page',
                           name='ShowChoice'),
                       url(r'ShowAnswer/$',
                           'Test3.views.ShowAnswer_page',
                           name='ShowAnswer'),
                       url(r'history/$',
                           'Test3.views.history_page',
                           name='history'),
                       url(r'nonselect/$',
                           'Test3.views.select_page',
                           name='nonselect')
                     #  (r'^static/(?P<path>.*)$',
                     #  'django.views.static.serve',
                     #  {'document_root': settings.STATIC_ROOT}
                     )
